const express = require('express');
const fs = require('fs');
const user = require('./user.json');
const app = express();
const port = 7000;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

function writeFile(jsonData) {
    fs.writeFile("user.json", jsonData, function(err) {
        if (err) {
            console.log(err);
        }
    });
}

app.get("/info",(req,res)=>{
    res.status(200).send({
        Infomation:user
    })
});


app.post("/addinginfo",(req,res)=>{
    try {
        const {name,lastname,employee_id,position,telephone,email} = req.body;
        user.forEach(info => {
            if (info.email === email || info.telephone === telephone) {
                return res.status(400).send("Bad request");
            }
        });
        user.push(req.body);
        writeFile(JSON.stringify(user));
        return res.status(201).send("Sucess");
    } catch (err) {
        res.status(400).send(err);
    }
});

app.put("/update/:employeeID",(req,res)=>{
    const {employeeID} = req.params;
    try {
        const {name,lastname,employee_id,position,telephone,email} = req.body;
        user.forEach(info => {
            if (info.email === email || info.telephone === telephone) {
                return res.status(400).send("Bad request");
            }
        });
        const index = user.findIndex((e)=>{
            return Number(e.employee_id) === Number(employeeID);
        })
        if (index != -1) {
            user[index] = req.body;
            writeFile(JSON.stringify(user));
            return res.status(201).send("Success");
        } else {
            return res.status(400).send("Invalid Data");
        }
    } catch (err) {
        res.status(400).send(err);
    }
});

app.delete('/delete/:employeeID',(req,res)=>{
    try{
        const {employeeID} = req.params;
        const result = user.findIndex((e)=>{
            return Number(e.employee_id) === Number(employeeID);
        })
        if ( result === -1){
            return res.status(400).send("Invalid Data");
        } else {
            user.splice(result,1);
            writeFile(JSON.stringify(user));
            return res.status(200).send("Success");
        }
    } catch (err) {
        res.status(400).send(err);
    }
})

app.listen(port,()=>{
    console.log("server is running");
});
