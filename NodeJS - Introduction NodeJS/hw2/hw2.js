var alphabet = [
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z"]

function print(value) {
    try {
        let out = getValue(value);
        return out
    } catch (error) {
        console.log("Error message: " + error.message)
    }
}

function getValue(value) {
    return new Promise((resolve, reject) => {
        resolve(value);
    });
}

function main() {
    alphabet.forEach(
        async function (value , i) {
            if ( i & 1 ){
                const [result1, result2] = await Promise.all([print(value), print(alphabet[i - 1])]);
                console.log(result1)
                console.log(result2)
            }
        }
    )
}

main()
