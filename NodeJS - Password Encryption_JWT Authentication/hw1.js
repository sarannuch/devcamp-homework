const express = require("express");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const secret = "SecretPassword";

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:false}));

const users = [
    {
        id: 1,
        username: "Lily",
        email: "lily@email.com",
        password: "$2a$12$Ru4GdtRmkcbD0zCa.EQ4VeRt/451o2zLhbHc91cT0I.b1NNkHZZn",
        // password = 123456
    },
];

function checkDuplicate(variable, newInput) {
    const uniqueValues = new Set(users.map((user) => user[variable]));
    if (uniqueValues.has(newInput)) {
        return true;
    }
};

app.get("/users",(req,res,next)=>{
    res.status(200).send({
        users:users
    });
});

app.post("/register",(req,res,next)=>{
    if( req.body.username == null || req.body.password == null || req.body.email == null ) {
        return res.status(201).send({
            message:"data's missing"
        });
    }

    if (
        checkDuplicate("username", req.body.username) ||
        checkDuplicate("email", req.body.email)
    ) {
        return res.status(201).send("user is exist!");
    }
    bcrypt.genSalt(10, (err,salt) => {
        bcrypt.hash(req.body.password, salt, (err,hash)=>{
            let newUser = {
                id: users.length +1,
                username: req.body.username,
                email: req.body.email,
                password: hash,
            };
            users.push(newUser);
        });
    });
    console.log(users);
    res.status(200).send("Completed");
});

app.post("/login", (req, res, next) => {
    try {
        const index = users.findIndex((user) => (user.username == req.body.username));
        bcrypt.compare(req.body.password, users[index].password, function(err, isLogin) {
            if (isLogin) {
                const token = jwt.sign({ username: users[index].username, email: users[index].email }, secret);
                return res.status(200).json({message: "Login successful", token});
            } else {
                return res.status(201).json({message: "Login failed"});
            }
        });            
    } catch {
        return res.status(202).send("User Not Found");
    }
});

app.post("/authen", (req, res, next) => {
    const token = req.headers.authorization.split(' ')[1];
    try {
        var decoded = jwt.verify(token, secret);
        return res.status(200).json({decoded});
    } catch {
        return res.status(201).json("invalid token!");
    }
    
});

app.listen(3123, () => console.log("Server started on port 3123"));
