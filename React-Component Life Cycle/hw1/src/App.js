import { Component } from "react";
import "./styles.css";

export default class App extends Component {
  state = {
    lists: [],
    loading: false
  };

  async componentDidMount() {
    this.setState({
      loading: true
    })
    const response = await fetch('https://todo.showkhun.co/lists')
    const data = await response.json()
    this.setState({
      lists: data.lists,
      loading: false
    })
  }

  render() {
    return (
      <div className="App">
        <h1>Hello TODOLIST</h1>
        {this.state.loading ? (
          <h1>Loading...</h1>
        ) : (
          false
        )}
        <ul>
          {
            this.state.lists.map((todo) =>
            <li key={todo.id}><a>ID: {todo.todo.id} Name: {todo.todo} Detail: {todo.detail} Status: {todo.status}</a></li>)
          }
        </ul>
      </div>
    );
  }
}