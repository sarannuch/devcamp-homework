import './App.css';
import Main from "./components/Main";
import Create from "./components/Create";
import Lists from "./components/Lists";
import Navbar from './components/Navbar';

import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useState } from "react";

function App() {
  const [todo, settodo] = useState([])
  return (
    <div>
     <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path='/' index element={<Main />} />
          <Route path='/create' element={<Create todos={todo} setTodos={settodo} />} />
          <Route path='/lists' element={<Lists todos={todo}/>} />
        </Routes>
     </BrowserRouter>
   </div>
  );
}

export default App;
