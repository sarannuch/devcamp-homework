function Create(props) {

    const handleSubmit = (e) => {
        e.preventDefault();
        props.setTodos([...props.todos, [e.target[0].value, e.target[1].value]]);
    };

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    Todo:
                    <input type="text" name="item" />
                </label>
                <label>
                    Detail:
                    <textarea name="detail" />
                </label>
                <input type="submit" value="Submit" />
            </form>
        </div>
    )
}

export default Create;