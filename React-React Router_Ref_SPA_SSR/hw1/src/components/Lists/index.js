function List(props) {
    return (
        <div>
            <ul>
                {
                    props.todos.map((todo) => 
                        <li key={Math.random()}>Todo: {todo[0]} Detail: {todo[1]}</li>
                    )
                }
            </ul>
        </div>
    )
}

export default List;