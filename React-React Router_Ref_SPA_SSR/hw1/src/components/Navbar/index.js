import { useNavigate } from "react-router-dom";
import "./Navbar.css"

function Navbar () {
    
    const navigate = useNavigate()
    
    return (
        <div className="navbar">
            <div className="nav" onClick={() => navigate('/')}>Main</div>
            <div className="nav" onClick={() => navigate('/create')}>Create</div>
            <div className="nav" onClick={() => navigate('/lists')}>Lists</div>
        </div>
    )
}

export default Navbar;