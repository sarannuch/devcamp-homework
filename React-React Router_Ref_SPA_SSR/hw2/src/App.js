import './App.css';
import Main from "./components/Main";
import Create from "./components/Create";
import Lists from "./components/Lists";
import Navbar from './components/Navbar';

import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { useState } from "react";

function App() {
  const [items, setItem] = useState([])
  return (
    <div>
     <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path='/' index element={<Main />} />
          <Route path='/create' element={<Create items={items} setItem={setItem} />} />
          <Route path='/lists' element={<Lists items={items}/>} />
        </Routes>
     </BrowserRouter>
   </div>
  );
}

export default App;
