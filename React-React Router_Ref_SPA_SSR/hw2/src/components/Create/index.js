import { useNavigate } from "react-router-dom";

function Create(props) {

    const navigate = useNavigate()

    const handleSubmit = (e) => {
        e.preventDefault();
        createData(e.target[0].value, e.target[1].value)
    };

    const createData = async (todo, detail) => {
        try {
            const res = await fetch("https://todo.showkhun.co/create", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    todo,
                    detail
                })
            });

            if(res.ok){
                navigate('/lists')
            }
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <div>
            <form onSubmit={handleSubmit}>
                <label>
                    Todo:
                    <input type="text" name="item" />
                </label>
                <label>
                    Detail:
                    <textarea name="detail" />
                </label>
                <input type="submit" value="Add Todo" />
            </form>
        </div>
    )
}

export default Create;