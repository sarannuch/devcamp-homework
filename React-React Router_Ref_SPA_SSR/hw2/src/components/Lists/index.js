import { Component } from "react";

export default class Lists extends Component {
    state = {
        lists: []
    };

    async componentDidMount() {
        const response = await fetch('https://todo.showkhun.co/lists')
        const data = await response.json()
        this.setState({
            lists: data.lists,
        })
    }

    render() {
        return (
            <div>
                <ul>
                    {
                        this.state.lists.map((todo) => 
                            <li key={todo.id}>Todo: {todo.todo} Detail: {todo.detail}</li>
                        )
                    }
                </ul>
            </div>
        );
    }

}