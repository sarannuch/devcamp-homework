import React from 'react'
import PropTypes from 'prop-types'
import * as actions from '../actions'
import { connect } from 'react-redux'

const mapStateToProps = (state, ownProps) => ({
  todos: state.todos
})

const TodoItem = props => (
  <li>
    <div className="view">
      <label>{props.todo.text}</label>
      <button
        className="destroy"
        onClick={() => props.deleteTodo(props.todo.id)}
      />
    </div>
  </li>
)

TodoItem.defaultProps = {
  deleteTodo: () => alert('Give me some function!')
}

TodoItem.propTypes = {
  deleteTodo: PropTypes.func.isRequired,
  todo: PropTypes.shape({
    id: PropTypes.number,
    text: PropTypes.string
  }).isRequired
}

// TODO: connect to Redux store for dispatch action
export default connect(mapStateToProps, actions)(TodoItem)
