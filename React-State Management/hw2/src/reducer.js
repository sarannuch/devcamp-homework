const initState = {
    userlist: [],
    cid: 0
  }
  
  export default function rootReducer(state = initState, action) {
    switch (action.type) {
      case 'ADD':
        return {
          ...state,
          userlist: [
            ...state.userlist,
            {
              id: action.id,
              email: action.email,
              gender: action.gender,
              fullname: action.fullname,
              picture: action.picture
            }
          ]
        }
      case 'SET_ID':
        return {
          ...state,
          cid: action.cid
        }
      default:
        return state
    }
  }
  